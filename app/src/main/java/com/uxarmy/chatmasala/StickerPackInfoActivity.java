/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.uxarmy.chatmasala;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class StickerPackInfoActivity extends BaseActivity {

    private static final String TAG = "StickerPackInfoActivity";
    CardView cv1,cv2,cv3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_page);
        cv1=(CardView)findViewById(R.id.card_view1);
        cv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=com.uxarmy.chatmasala");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);


            }
        });
        cv2=(CardView)findViewById(R.id.card_view2);
        cv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=com.uxarmy.chatmasala");
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=com.uxarmy.chatmasala" )));
                }

            }
        });
        cv3=(CardView)findViewById(R.id.card_view3);
        cv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"hi@uxarmy.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Suggestions for Sticker app");
                i.putExtra(Intent.EXTRA_TEXT   , "I want to suggest");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

//        final String trayIconUriString = getIntent().getStringExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_TRAY_ICON);
//        final String website = getIntent().getStringExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_WEBSITE);
//        final String email = getIntent().getStringExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_EMAIL);
//        final String privacyPolicy = getIntent().getStringExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_PRIVACY_POLICY);
//
//        final TextView trayIcon = findViewById(R.id.tray_icon);
//        try {
//            final InputStream inputStream = getContentResolver().openInputStream(Uri.parse(trayIconUriString));
//            final BitmapDrawable trayDrawable = new BitmapDrawable(getResources(), inputStream);
//            final Drawable emailDrawable = getDrawableForAllAPIs(R.drawable.sticker_3rdparty_email);
//            trayDrawable.setBounds(new Rect(0, 0, emailDrawable.getIntrinsicWidth(), emailDrawable.getIntrinsicHeight()));
////            trayIcon.setCompoundDrawables(trayDrawable, null, null, null);
//        } catch (FileNotFoundException e) {
//            Log.e(TAG, "could not find the uri for the tray image:" + trayIconUriString);
//        }
//
//        final TextView viewWebpage = findViewById(R.id.view_webpage);
//        if (TextUtils.isEmpty(website)) {
//            viewWebpage.setVisibility(View.GONE);
//        } else {
//            viewWebpage.setOnClickListener(v -> launchWebpage(website));
//        }
//
//        final TextView sendEmail = findViewById(R.id.send_email);
//        if (TextUtils.isEmpty(email)) {
//            sendEmail.setVisibility(View.GONE);
//        } else {
//            sendEmail.setOnClickListener(v -> launchEmailClient(email));
//        }
//
//        final TextView viewPrivacyPolicy = findViewById(R.id.privacy_policy);
//        if (TextUtils.isEmpty(privacyPolicy)) {
//            viewPrivacyPolicy.setVisibility(View.GONE);
//        } else {
//            viewPrivacyPolicy.setOnClickListener(v -> launchWebpage(privacyPolicy));
//        }
//    }
//
//    private void launchEmailClient(String email) {
//        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//                "mailto", email, null));
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
//        startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.info_send_email_to_prompt)));
//    }
//
//    private void launchWebpage(String website) {
//        Uri uri = Uri.parse(website);
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        startActivity(intent);
//    }
//
//    private Drawable getDrawableForAllAPIs(@DrawableRes int id) {
//        if (Build.VERSION.SDK_INT >= 21) {
//            return getDrawable(id);
//        } else {
//            return getResources().getDrawable(id);
//        }
    }
}
